/* eslint-disable no-undef */
function main(min, max) {
    let j = 0;
    for (let i = min; i <= max; i++) j += i * (i - (i % 2 === 0 ? 2 : 1));
    return j;
}

console.log(main(3, 1000));
