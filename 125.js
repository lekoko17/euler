/* eslint-disable no-undef */

function main(maxNumber) {
    const amount = [];
    let index = 1;
    while (index <= maxNumber ** 0.5) {
        amount.push(...(findPalindromic(index, maxNumber) ?? []));
        index++;
    }
    return [...new Set(amount)].reduce((accumulator, currentValue) => accumulator + currentValue, 0);
}

function findPalindromic(startIndex, maxNumber) {
    const palindromic = [];
    let number = startIndex ** 2;
    let index = startIndex + 1;
    while (number + index ** 2 <= maxNumber) {
        number += index ** 2;
        index++;
        if ('' + number === ('' + number).split('').reverse().join('')) palindromic.push(number);
    }
    return palindromic;
}

console.log(main(10 ** 8));
