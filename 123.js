/* eslint-disable no-undef */
function main(limit) {
    const p = getPrimes(Math.sqrt(limit) * 10);
    for (let n = 1; n <= p.length; n++) {
        if (n % 2 == 0) continue;
        const r = p[n - 1] * n * 2;
        if (r > limit) return n;
    }
}

function getPrimes(limit) {
    const array = [];
    const p = Math.floor((limit - 1) / 2);
    const l = Math.floor((Math.floor(Math.sqrt(limit)) - 1) / 2);
    for (let i = 1; i <= l; i++)
        if (!array[i - 1]) {
            let j = 2 * i * (i + 1);
            while (j <= p) {
                array[j - 1] = true;
                j += 2 * i + 1;
            }
        }
    const result = [2];
    for (let i = 1; i <= p; i++) if (!array[i - 1]) result.push(2 * i + 1);
    return result;
}

console.log(main(10 ** 10));
